# GitLab Container Scanning

## DEPRECATED

This project was actually never used and instead the Container Scanning capabilities are currently covered by [the Klar analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/klar)